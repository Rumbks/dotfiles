execute pathogen#infect()
set expandtab
set textwidth=80
set ts=2
set shiftwidth=2
set autoindent
set smartindent

let mapleader = "\<Space>" 
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'
cnoremap <expr> NT getcmdtype() == ':' ? 'NERDTree' : 'NT'

colorscheme gruvbox
set background=dark
let g:gruvbox_italic='0'

set guifont=Droid_Sans_Mono_Slashed:h14:cRUSSIAN:qDRAFT

syntax on
set number
highlight ColorColumn ctermbg=darkgray

set ic
set hls

set wildmode=longest:list,full

set fileencoding=utf-8
set encoding=utf-8

function Cmp()
  :write
  :!gcc % -o program
  :!./program
endfunction

function Build()
	:w
	:!cmake -DCMAKE_BUILD_TYPE=Debug -G "CodeBlocks - MinGW Makefiles" .
	:!cmake --build . --target all
endfunction


"Easymotion

let g:EasyMotion_do_mapping = 0 " Disable default mappings

nmap s <Plug>(easymotion-overwin-f2)

" Turn on case-insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

nmap <Leader>; <Plug>(easymotion-next)
nmap <Leader>, <Plug>(easymotion-prev)

nmap s <Plug>(easymotion-overwin-f)
nmap <Leader>s <Plug>(easymotion-overwin-f2)
map <Leader>f <Plug>(easymotion-f)
map <Leader>t <Plug>(easymotion-t)
"Syntastic
let g:syntastic_cpp_compiler_options = ' --std=c++11'
